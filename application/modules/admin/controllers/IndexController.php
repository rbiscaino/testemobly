<?php

use Ecommerce\Model\User;

class Admin_IndexController extends Zend_Controller_Action
{
    private $usuario;

    public function init()
    {
        $auth = Zend_Auth::getInstance();
        if($auth->hasIdentity()){
            $this->usuario = $auth->getIdentity();
        }
    }

    public function indexAction()
    {
        
    }
    
    public function loginAction()
    {
        $form = new Admin_Form_Login();
        
        $this->view->form = $form;
        
        $request = $this->getRequest();
        $data = $request->getParams();
        
        if ($request->isPost()) {
            if($form->isValid($data)) {
                if($user = \Ecommerce\Auth::autenticar($data['email'], $data['senha'])) {
                    
                    $this->redirect('/admin/index');
                }
            }
        }
    }
    
    public function logoutAction()
    {
        \Zend_session::destroy();
        $this->redirect('/admin');
    }


}

