<?php


/**
 * Description of UsuarioController
 *
 * @author rodolfo
 */
class Admin_UsuarioController extends Zend_Controller_Action
{
    private $usuario;
    private $_flashMessenger;
    
    public function init()
    {
        $auth = Zend_Auth::getInstance();
        if($auth->hasIdentity()){
            $this->usuario = $auth->getIdentity();
        }
        
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');        
        $this->view->msgFlashMessenger = $this->_flashMessenger->getMessages();
    }
    
    public function indexAction()
    {
        $arr_users = \Ecommerce\Model\User::listAll();
        
        $this->view->arr_users = $arr_users;
    }
    
    public function cadastrarAction()
    {        
        $form = new Admin_Form_Usuario();        
        
        $request = $this->getRequest();
        $data = $request->getParams();        
        
        if ($request->isPost()) {
            if($form->isValid($data)) {
                
                if(isset($data['id']) && !empty($data['id'])) {
                    $usuario = \Ecommerce\Model\User::findById($data['id']);
                    $perfil = \Ecommerce\Model\Perfil::findById($usuario->getPerfilId());
                    if(!$perfil instanceof \Ecommerce\Model\Perfil) {
                        $perfil = new \Ecommerce\Model\Perfil();
                    }
                } else {
                    $usuario = new \Ecommerce\Model\User();
                    $perfil = new \Ecommerce\Model\Perfil();
                }
                
                $perfil->setLogradouro($data['logradouro']);
                $perfil->setNumero($data['numero']);
                $perfil->setComplemento($data['complemento']);
                $perfil->setCep($data['cep']);
                $perfil->setCidadeId($data['cidade']);
                
                $perfil = $perfil->gravar();
                
                $usuario->setPerfilId($perfil->getId());
                $usuario->setEmail($data['email']);
                $usuario->setNome($data['nome']);
                $usuario->setPassword(md5($data['senha']));
                $usuario->setGrupoId(1);
                
                $usuario->gravar();
                
                $this->_flashMessenger->addMessage('Usuário gravado com sucesso');
                
                
        
            }
        } else {
            if(isset($data['id']) && !empty($data['id'])) {
                $usuario = \Ecommerce\Model\User::findById($data['id']);
                if($usuario instanceof \Ecommerce\Model\User) {
                    $form->populate($usuario->toArray());
                }
                
                $this->_flashMessenger->addMessage('Usuário alterado com sucesso');
                
            }
        }
        
        
        
        $this->view->form = $form;
    }
}
