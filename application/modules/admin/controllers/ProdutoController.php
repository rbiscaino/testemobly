<?php


/**
 * Description of ProdutoController
 *
 * @author rodolfo
 */
class Admin_ProdutoController extends Zend_Controller_Action
{
    private $usuario;
    
    private $_flashMessenger;
    
    public function init()
    {
        $auth = Zend_Auth::getInstance();
        if($auth->hasIdentity()){
            $this->usuario = $auth->getIdentity();
        }
        
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');        
        $this->view->msgFlashMessenger = $this->_flashMessenger->getMessages();
    }
    
    public function indexAction()
    {
        $arr_produtos = \Ecommerce\Model\Produto::listAll();
        
        $this->view->arr_produtos = $arr_produtos;
    }
    
    public function cadastrarAction()
    {        
        $form = new Admin_Form_Produto();        
        
        $request = $this->getRequest();
        $data = $request->getParams();        
        
        if ($request->isPost()) {
            if($form->isValid($data)) {
                
                if(isset($data['id']) && !empty($data['id'])) {
                    $produto = \Ecommerce\Model\Produto::findById($data['id']);
                } else {
                    $produto = new \Ecommerce\Model\Produto();
                }                
                
                if ($form->file->isUploaded() && $form->file->receive()) {
                    
                    if($produto->getImagem()) {
                        unlink($produto->getImagem());
                    }                    
                    
                    $fileName = $form->file->getFileName(null, false);
                    $fileExtension = $this->getFileExtension($fileName);
                    
                    $newName = APPLICATION_PATH. "/../public/imagens/".rand(2,2000).'-'.md5(time()). '.' . $fileExtension;
                    
                    rename(APPLICATION_PATH. "/../public/imagens/".$fileName, $newName);
                    
                    $produto->setImagem($newName);
                }
               
                $produto->setNome($data['nome']);
                $produto->setDescricao($data['descricao']);
                $produto->setPreco($data['preco']);                
                
                $produto->gravar();
                
                $categorias = $data['categoria'];
                
                $produto->removerCategorias();
                foreach($categorias as $value) {
                    $produto->addCategoria($value);
                }
                
                \Ecommerce\MemcacheMutex::mutexAction('save', $produto->gerarChaveCache(), $produto);
                
                $this->_flashMessenger->addMessage('Produto gravado com sucesso');
        
            }
        } else {
            if(isset($data['id']) && !empty($data['id'])) {
                $produto = \Ecommerce\Model\Produto::findById($data['id']);
                if($produto instanceof \Ecommerce\Model\Produto) {
                    
                    $arr_categorias = \Ecommerce\Model\Categoria::findByProdutoId($produto->getId());
                    $arr_categorias_tmp = array();
                    foreach($arr_categorias as $categoria) {
                        $arr_categorias_tmp[] = $categoria->getId();
                    }
                    
                    $form->getElement('categoria')->setValue($arr_categorias_tmp);
                    
                    $form->populate($produto->toArray());
                    
                    $this->_flashMessenger->addMessage('Produto alterado com sucesso');
                }
                
            }
        }        
        
        $this->view->form = $form;
    }
    
    public function getFileExtension($fileName)
    {
      $fa = explode(".",$fileName);
      return (sizeof($fa) > 1) ? strtolower(end($fa)): "";
    }
}
