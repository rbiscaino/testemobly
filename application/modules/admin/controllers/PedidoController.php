<?php


/**
 * Description of PedidoController
 *
 * @author rodolfo
 */
class Admin_PedidoController extends Zend_Controller_Action
{
    private $usuario;
    
    public function init()
    {
        $auth = Zend_Auth::getInstance();
        if($auth->hasIdentity()){
            $this->usuario = $auth->getIdentity();
        }
    }
    
    public function indexAction()
    {
        $arr_pedidos = \Ecommerce\Model\Pedido::listAll();
        
        $this->view->arr_pedidos = $arr_pedidos;
    }
}
