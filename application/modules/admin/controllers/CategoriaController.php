<?php


/**
 * Description of CategoriaController
 *
 * @author rodolfo
 */
class Admin_CategoriaController extends Zend_Controller_Action
{
    private $usuario;
    private $_flashMessenger;
    
    public function init()
    {
        $auth = Zend_Auth::getInstance();
        if($auth->hasIdentity()){
            $this->usuario = $auth->getIdentity();
        }
        
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');        
        $this->view->msgFlashMessenger = $this->_flashMessenger->getMessages();
    }
    
    public function indexAction()
    {
        $arr_categorias = \Ecommerce\Model\Categoria::listAll();
        
        $this->view->arr_categorias = $arr_categorias;
    }
    
    public function cadastrarAction()
    {        
        $form = new Admin_Form_Categoria();        
        
        $request = $this->getRequest();
        $data = $request->getParams();        
        
        if ($request->isPost()) {
            if($form->isValid($data)) {
                
                if(isset($data['id']) && !empty($data['id'])) {
                    $categoria = \Ecommerce\Model\Categoria::findById($data['id']);
                } else {
                    $categoria = new \Ecommerce\Model\Categoria();
                }
               
                $categoria->setNome($data['nome']);
                $categoria->setDescricao($data['descricao']);
                
                $categoria->gravar();
                
                $this->_flashMessenger->addMessage('Categoria gravada com sucesso');
        
            }
        } else {
            if(isset($data['id']) && !empty($data['id'])) {
                $categoria = \Ecommerce\Model\Categoria::findById($data['id']);
                if($categoria instanceof \Ecommerce\Model\Categoria) {
                    $form->populate($categoria->toArray());
                }
                
                $this->_flashMessenger->addMessage('Categoria alterada com sucesso');
                
            }
        }
        
        
        
        $this->view->form = $form;
    }
}
