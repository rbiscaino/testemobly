<?php

/**
 * Description of usuario
 *
 * @author rodolfo
 */
class Admin_Form_Usuario extends Zend_Form
{
    public function init()
    {
        $this->setName("form_usuario");
        $this->setAttrib('id', 'form_usuario');
        $this->setAttrib('class', 'zend_form');

        $this->addElement('hash', 'no_csrf_usuario', array('timeout' => '1800', 'salt' => 'unique'));

        $form = new Zend_Form();

        $id = $form->createElement('hidden', 'id', array(
            'decorators' => array('ViewHelper'),
        ));

        $form_tipo = $form->createElement('hidden', 'form_tipo', array(
            'decorators' => array('ViewHelper'),
            'value' => 'form_usuario',
        ));
        
        $nome = $form->createElement('text', 'nome', array(
            'label' => 'Nome',
            'required' => true,
        ))->addFilter('StringTrim');
        
        $email = $form->createElement('text', 'email', array(
            'label' => 'E-mail',
            'required' => true,
        ))->addFilter('StringTrim');
        
        $senha = $form->createElement('text', 'senha', array(
            'label' => 'Senha',
            'required' => true,
        ))->addFilter('StringTrim');
        
        $cep = $form->createElement('text', 'cep', array(
            'label' => 'CEP',
            'required' => true,
            'maxlength' => 10
        ))->addFilter('StringTrim');
        
        $logradouro = $form->createElement('text', 'logradouro', array(
            'label' => 'Endereço',
            'required' => true,
            'maxlength' => 200
        ))->addFilter('StringTrim');
        
        $numero = $form->createElement('text', 'numero', array(
            'label' => 'Número',
            'required' => true,
            'maxlength' => 10
        ))->addFilter('StringTrim');
        
        $complemento = $form->createElement('text', 'complemento', array(
            'label' => 'Complemento',
            'maxlength' => 100
        ))->addFilter('StringTrim');
        
        $cidades_exemplo = array(
            '1' => 'São Paulo',
            '2' => 'Rio de Janeiro',
            '3' => 'Salvador'
        );

        $cidade = $form->createElement('select', 'cidade', array(
            'label' => 'Cidade',
            'required' => true,
        ))
        ->addMultiOptions($cidades_exemplo);
        
        $btn_submit = $form->createElement('submit', 'salvar', array(
            'label' => 'Salvar'
        ));
        
        $btn_cancel = $form->createElement('button', 'cancelar', array(
            'label' => 'Cancelar'
        ))->setAttrib('onclick', 'javascript: window.history.back();');
        
        $this->addElement($id)
                ->addElement($form_tipo)
                ->addElement($nome)
                ->addElement($email)
                ->addElement($senha)
                ->addElement($cep)
                ->addElement($logradouro)
                ->addElement($numero)
                ->addElement($complemento)
                ->addElement($cidade)
                ->addElement($btn_submit)
                ->addElement($btn_cancel);
    }
}
