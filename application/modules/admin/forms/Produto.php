<?php

/**
 * Description of produto
 *
 * @author rodolfo
 */
class Admin_Form_Produto extends Zend_Form
{
    public function init()
    {
        $this->setName("form_produto");
        $this->setAttrib('id', 'form_produto');
        $this->setAttrib('class', 'zend_form');

        $this->addElement('hash', 'no_csrf_produto', array('timeout' => '1800', 'salt' => 'unique'));

        $form = new Zend_Form();

        $id = $form->createElement('hidden', 'id', array(
            'decorators' => array('ViewHelper'),
        ));
        
        $nome = $form->createElement('text', 'nome', array(
            'label' => 'Nome',
            'required' => true,
        ))->addFilter('StringTrim');
        
        $descricao = $form->createElement('textarea', 'descricao', array(
            'label' => 'Descrição',
            'required' => true,
        ))->addFilter('StringTrim');
        
        $fileElement = new Zend_Form_Element_File('file');
        $fileElement->setLabel('Imagem');
        $fileElement->addValidator('Extension', false, 'jpg,jpeg,png,gif');
        $fileElement->addValidator('Size', false, 2048000);
        $fileElement->addValidator('Count', false, 1);
        $fileElement->setDestination(APPLICATION_PATH. "/../public/imagens/");
        $fileElement->setRequired(false);
        
        
        $preco = $form->createElement('text', 'preco', array(
            'label' => 'Preço',
            'required' => true,
        ))->addFilter('StringTrim');
        
        $categorias = $this->carregarCategorias();
        
        $categoria = $form->createElement('select', 'categoria[]', array(
            'label' => 'Categoria',
            'required' => true,
            'multiple' => true
        ))
        ->addMultiOptions($categorias)
        ->setRegisterInArrayValidator(false);
        
        $btn_submit = $form->createElement('submit', 'salvar', array(
            'label' => 'Salvar'
        ));
        
        $btn_cancel = $form->createElement('button', 'cancelar', array(
            'label' => 'Cancelar'
        ))->setAttrib('onclick', 'javascript: window.history.back();');
        
        $this->addElement($id)
                ->addElement($nome)
                ->addElement($descricao)
                ->addElement($fileElement)
                ->addElement($preco)
                ->addElement($categoria)
                ->addElement($btn_submit)
                ->addElement($btn_cancel);
    }
    
    private function carregarCategorias()
    {
        $categorias = \Ecommerce\Model\Categoria::listAll();
        $arr_categorias = array();
        
        if($categorias) {
            foreach($categorias as $categoria) {
                $arr_categorias[$categoria->getId()] = $categoria->getNome();
            }
        }
        
        return $arr_categorias;        
    }
}
