<?php

/**
 * Description of login
 *
 * @author rodolfo
 */
class Admin_Form_Login extends Zend_Form
{
    public function init()
    {
        $this->setName("form_login");
        $this->setAttrib('id', 'form_login');
        $this->setAttrib('class', 'zend_form');

        $this->addElement('hash', 'no_csrf_login', array('timeout' => '1800', 'salt' => 'unique'));

        $form = new Zend_Form();

        $id = $form->createElement('hidden', 'id', array(
            'decorators' => array('ViewHelper'),
        ));

        $form_tipo = $form->createElement('hidden', 'form_tipo', array(
            'decorators' => array('ViewHelper'),
            'value' => 'form_login'
        ));        
        
        $email = $form->createElement('text', 'email', array(
            'label' => 'E-mail',
            'required' => true,
        ))->addFilter('StringTrim');
        
        $senha = $form->createElement('password', 'senha', array(
            'label' => 'Senha',
            'required' => true,
        ))->addFilter('StringTrim');
        
        $btn_submit = $form->createElement('submit', 'sigin', array(
            'label' => 'Sigin'
        ));
        
        $this->addElement($id)
                ->addElement($form_tipo)
                ->addElement($email)
                ->addElement($senha)
                ->addElement($btn_submit);
    }
}
