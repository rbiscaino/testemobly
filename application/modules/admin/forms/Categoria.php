<?php

/**
 * Description of categoria
 *
 * @author rodolfo
 */
class Admin_Form_Categoria extends Zend_Form
{
    public function init()
    {
        $this->setName("form_categoria");
        $this->setAttrib('id', 'form_categoria');
        $this->setAttrib('class', 'zend_form');

        $this->addElement('hash', 'no_csrf_categoria', array('timeout' => '1800', 'salt' => 'unique'));

        $form = new Zend_Form();

        $id = $form->createElement('hidden', 'id', array(
            'decorators' => array('ViewHelper'),
        ));
        
        $nome = $form->createElement('text', 'nome', array(
            'label' => 'Nome',
            'required' => true,
        ))->addFilter('StringTrim');
        
        $descricao = $form->createElement('textarea', 'descricao', array(
            'label' => 'Descrição',
            'required' => true,
        ))->addFilter('StringTrim');

        $btn_submit = $form->createElement('submit', 'salvar', array(
            'label' => 'Salvar'
        ));
        
        $btn_cancel = $form->createElement('button', 'cancelar', array(
            'label' => 'Cancelar'
        ))->setAttrib('onclick', 'javascript: window.history.back();');
        
        $this->addElement($id)
                ->addElement($nome)
                ->addElement($descricao)
                ->addElement($btn_submit)
                ->addElement($btn_cancel);
    }
}
