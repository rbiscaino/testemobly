<?php

class Site_LoginController extends Zend_Controller_Action
{
    
    private $_sessao;
    
    public function init()
    {
       $this->_sessao = new Zend_Session_Namespace('sessao');
       
       if ($this->_helper->FlashMessenger->hasMessages()) {
            $this->view->messages = $this->_helper->FlashMessenger->getMessages();
        }
    }

    public function indexAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        $request = $this->getRequest();
        $data = $request->getParams();
        
        if ($request->isPost()) {
            if(isset($data['email']) && !empty($data['email']) && isset($data['password']) && !empty($data['password'])) {
                if($user = \Ecommerce\Auth::autenticar($data['email'], $data['password'])) {                                        
                    $user_tmp = $user->toArray();
                    unset($user_tmp['password']);
                    
                    $this->_helper->json(array('success'=> true, 'user'=> $user_tmp));
                } else {
                    $this->_helper->json(array('success'=> false, 'message' => 'Usuário não existe'));
                }
            } else {
                $this->_helper->json(array('success'=> false, 'message' => 'Preencher todos os campos'));
            }
            
            
            return;
        }
        
    }


}

