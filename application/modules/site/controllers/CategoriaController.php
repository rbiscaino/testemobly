<?php

class Site_CategoriaController extends Zend_Controller_Action
{
    
    private $usuario;
    
    public function init()
    {
        $auth = Zend_Auth::getInstance();
        if($auth->hasIdentity()){
            $this->usuario = $auth->getIdentity();
        }
    }

    public function indexAction()
    {
        $id = $this->_getParam('id', 0);
        
        $categoria = \Ecommerce\Model\Categoria::findById($id);
        if(!$categoria instanceof \Ecommerce\Model\Categoria) {
            $this->redirect('/');
        }
        
        $arr_produtos = \Ecommerce\Model\Produto::findByCategoriaId($id);
        
        $this->view->categoria = $categoria;
        $this->view->arr_produtos = $arr_produtos;
        
        $this->view->headScript()->appendFile("/js/addcarrinho.js"); 
    }


}

