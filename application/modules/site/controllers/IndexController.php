<?php

class Site_IndexController extends Zend_Controller_Action
{
    
    private $usuario;
    
    public function init()
    {
        $auth = Zend_Auth::getInstance();
        if($auth->hasIdentity()){
            $this->usuario = $auth->getIdentity();
        }
    }

    public function indexAction()
    {
        
    }
    
    public function carrinhoAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        $produto_id = $this->_getParam('produtoid', 0);
        $qtd = $this->_getParam('qtd', 0);
        
        $produto = \Ecommerce\Model\Produto::findById($produto_id);
        if(!$produto instanceof \Ecommerce\Model\Produto) {
            echo $this->_helper->json(array('success' => false, 'message' => 'Produto inválido'));
            return;
        }       
        
        $sessaocarrinho = new Zend_Session_Namespace('sessaocarrinho');
        $add=false;
        if(isset($sessaocarrinho->carrinho)) {
            foreach($sessaocarrinho->carrinho as $key => $carrinho) {
                if($carrinho['produto']->getId() == $produto_id) {
                    $carrinho['qtd'] += $qtd;
                    $sessaocarrinho->carrinho[$key] = $carrinho;
                    $add = true;
                    break;
                }
            }
            
            if(!$add) {
                $sessaocarrinho->carrinho[] = array(
                            'produto' => $produto,
                            'qtd' => $qtd
                );
            }
        } else {
            $sessaocarrinho->carrinho[] = array(
                        'produto' => $produto,
                        'qtd' => $qtd
            );
        }
        
        echo $this->_helper->json(array('success' => true, 'message' => 'Produto adicionado com sucesso'));
        
    }
    
    public function visualizarcarrinhoAction()
    {
        $sessaocarrinho = new Zend_Session_Namespace('sessaocarrinho');
        
        
        $this->view->carrinho = $sessaocarrinho->carrinho;
    }
    
    public function fecharpedidoAction()
    {
        $request = $this->getRequest();
        $data = $request->getParams();
        
        $form_login = new Admin_Form_Login();
        $form_usuario = new Admin_Form_Usuario();
        
        if ($request->isPost()) {
            if($data['form_tipo'] == 'form_login' ) {
                if($form_login->valid()) {
                    if($user = \Ecommerce\Auth::autenticar($data['email'], $data['senha'])) { 
                        $this->usuario = $user;
                    }
                }
            } else if($data['form_tipo'] == 'form_usuario') {
                if($form_usuario->valid()) {
                    $usuario = new \Ecommerce\Model\User();
                    $perfil = new \Ecommerce\Model\Perfil();
                    
                    $perfil->setLogradouro($data['logradouro']);
                    $perfil->setNumero($data['numero']);
                    $perfil->setComplemento($data['complemento']);
                    $perfil->setCep($data['cep']);
                    $perfil->setCidadeId($data['cidade']);

                    $perfil = $perfil->gravar();

                    $usuario->setPerfilId($perfil->getId());
                    $usuario->setEmail($data['email']);
                    $usuario->setNome($data['nome']);
                    $usuario->setPassword(md5($data['senha']));
                    $usuario->setGrupoId(1);

                    $usuario = $usuario->gravar();
                    
                    $this->usuario = $usuario;
                    
                    \Ecommerce\Auth::autenticar($data['email'], $data['senha']);
                }
            }
        } 
        
        if($this->usuario instanceof \Ecommerce\Model\User) {

           $sessaocarrinho = new Zend_Session_Namespace('sessaocarrinho');

           if(count($sessaocarrinho->carrinho) > 0) {            
                $pedido = new \Ecommerce\Model\Pedido();
                $pedido->setUserId($this->usuario->getId());

                $pedido = $pedido->gravar();

                foreach($sessaocarrinho->carrinho as $carrinho) {
                    $pedido->addProduto($carrinho['produto']->getId(), $carrinho['qtd']);
                }

                $this->view->mensagem = "Pedido finalizado com sucesso";
                $this->view->erro = false;

                Zend_Session::namespaceUnset('sessaocarrinho');
           } else {
                $this->view->mensagem = "Você não possui nenhum item no seu carrinho";
                $this->view->erro = false;
           }

        } else {
            $this->view->mensagem = "Favor efetuar o login";
            $this->view->form_login = $form_login;
            $this->view->form_usuario = $form_usuario;
            $this->view->erro = true;
        }
        
    }
    
    public function visualizarpedidoAction()
    {
        if($this->usuario instanceof \Ecommerce\Model\User) {
            $arr_pedidos = \Ecommerce\Model\Pedido::findByUserId($this->usuario->getId());
            
            $this->view->erro = false;
            $this->view->arr_pedidos = $arr_pedidos;
        } else {
            $this->view->erro = true;
            $this->view->mensagem = "Por favor efetue o login";
        }
    }
    
    public function buscarAction()
    {
        $nome = $this->_getParam('nome', '');
        $pagina = $this->_getParam('pag', 0);
        
        $arr_parametros = array(
            'nome' => $nome,
            'pagina' => $pagina
        );
        
        $this->view->paginator = \Ecommerce\Model\Produto::getProdutos($arr_parametros);        
        $this->view->nome = $nome;        
        $this->view->pagina = $pagina;        
        $this->view->headScript()->appendFile("/js/addcarrinho.js"); 
    }
    
    public function logoutAction() 
    {
        \Zend_session::destroy();
        $this->redirect('/site');
    }


}

