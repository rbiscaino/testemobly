<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initConfig()
    {
        $this->_regi = Zend_Registry::getInstance();
        $this->_opts = $this->getOption('options');
        $this->_resources = $this->getOption('resources');
		
        $config = new Zend_Config($this->getOptions());
		
        $this->_regi->set('config', $config);
        $this->_regi->set('options', $this->_opts);        
    }
    
    protected function _initDatabase()
    {
        $config = new Zend_Config($this->getOptions());
        
        $db = new \Ecommerce\DBMysql(true, $config->database->params->dbname, $config->database->params->host, $config->database->params->username, $config->database->params->password, 'utf8');
        
        $db->ThrowExceptions = true;
        
        return $db;
    }
    
    protected function _initPlugins()
    {
        $plugin_index = 1;
        $this->bootstrap('frontController');
        $front = $this->getResource('frontController');
        $front->registerPlugin(new Application_Plugin_Acl(), $plugin_index++);
        $front->registerPlugin(new Application_Plugin_Layout(), $plugin_index++);
    }

}
