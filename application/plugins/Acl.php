<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AclGrupos
 *
 * @author rodolfo
 */
class Application_Plugin_Acl extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $auth = Zend_Auth::getInstance();
        if($auth->hasIdentity()){
            $usuario = $auth->getIdentity();
            $grupoEntity = \Ecommerce\Model\Grupo::findById($usuario->getGrupoId());
            $grupo = $grupoEntity->getNome();
        }  else {
            $grupo = 'VISITANTE';
        }
        
        $zendAcl = new Zend_Acl();
        
        // usuario nao logado;
        $zendAcl->addRole(new Zend_Acl_Role('VISITANTE'));
        
        // usuario comum do sistema /site;
        $zendAcl->addRole(new Zend_Acl_Role('REGISTRADO'), 'VISITANTE');
        
        // usuario administrativo /admin;
        $zendAcl->addRole(new Zend_Acl_Role('ADMINISTRADOR'), 'REGISTRADO');
        
        $zendAcl->addResource('admin');
        $zendAcl->addResource('site');
        
        $zendAcl->allow('ADMINISTRADOR', 'admin');
        $zendAcl->allow('VISITANTE', 'admin', 'login');
        $zendAcl->allow('REGISTRADO', 'site');
        $zendAcl->allow('VISITANTE', 'site');
        
        $module	= $request->getModuleName();
        $controller = $request->getControllerName();
        $action	= $request->getActionName();
        
        if(!$zendAcl->hasRole($grupo) || !$zendAcl->has($module) || !$zendAcl->isAllowed($grupo,$module,$controller.'/'.$action)) {
//            throw new Exception("Access denied", 401);
            if($module == 'admin') {
                $request->setModuleName('admin')
                    ->setControllerName('index')
                        ->setActionName('login');
            } else {
                $request->setModuleName('site')
                    ->setControllerName('index');
//                    ->setActionName('index');
            }
        } 
    }
}
