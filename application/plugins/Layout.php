<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Layout
 *
 * @author rodolfo
 */
class Application_Plugin_Layout extends Zend_Controller_Plugin_Abstract
{

    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $view = Zend_Layout::getMvcInstance()->getView();
        $options = Zend_Registry::get('options');

        $module_name = $request->getModuleName();
        
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
        
        if($module_name == 'admin') {
            $view->headTitle()->exchangeArray(array($options['layout']['admin']['title']));
            $viewRenderer->view->layout()->setLayout('layout-admin');
        } else {            
            $view->headTitle()->exchangeArray(array($options['layout']['site']['title']));
        }        
        
    }
}