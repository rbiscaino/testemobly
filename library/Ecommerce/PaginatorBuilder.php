<?php
namespace Ecommerce;

class PaginatorBuilder
{
    public static function gerarSqlLimit($arr_parametros) {
        $pagina = (isset($arr_parametros['pagina']) ? $arr_parametros['pagina'] : 0 );
        $show_number = (isset($arr_parametros['show_number']) ? $arr_parametros['show_number'] : 25 );
        
        if ($pagina > 0) {
            $limite = floor(($pagina - 1) * $show_number);
        } else {
            $limite = $pagina;
        }
        return " LIMIT {$limite}, {$show_number}";
    }
    
    public static function paginarResultadosQuery($sqlQuery, $resultadosPagina, $arr_parametros) {
        $pagina = (isset($arr_parametros['pagina']) ? $arr_parametros['pagina'] : 0 );
        $show_number = (isset($arr_parametros['show_number']) ? $arr_parametros['show_number'] : 25 );
        $page_range = (isset($arr_parametros['page_range']) ? $arr_parametros['page_range'] : 15 );
        
        $sqlCount = "SELECT count(*) FROM (" . $sqlQuery . ") AS query_to_count";
        
        $front = \Zend_Controller_Front::getInstance();
        $db = $front->getParam('bootstrap')->getContainer()->database;
        
        $contador = $db->QuerySingleValue($sqlCount);
        
        $paginator_adapter = new Paginator($resultadosPagina, $contador);
        $paginator = new \Zend_Paginator($paginator_adapter);
        $paginator->setCurrentPageNumber($pagina);
        $paginator->setItemCountPerPage($show_number);
        $paginator->setPageRange($page_range);
        return $paginator;
    }
}

?>


