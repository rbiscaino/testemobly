<?php

namespace Ecommerce;

class MemcacheMutex
{

    public static function mutexAction($commandKey, $cachekey, $content = null, $expiration=false)
    {
        $memcached_host = "localhost";
        $memcached_port = "11211";

        $wcFrontendOptions = array(
            'lifetime' => ($expiration === false ? time() + 60 : $expiration ) ,
            'automatic_serialization' => true,
            'caching' => true,
        );

        $wcBackendOptions = array(
            'servers' => array(array(
                'host'   => $memcached_host,
                'port'   => $memcached_port,
                'weight' => 1
            )),
            'compression' => false
        );

        $cache = \Zend_Cache::factory('Core', 'Libmemcached', $wcFrontendOptions, $wcBackendOptions);

        if ($cachekey == "") {
            return false;
        }

        if ($commandKey == "load") {
            return $cache->load($cachekey);
        }

        if (($commandKey == "save") && ($content != null)) {
            return $cache->save($content, $cachekey);
        }

        if ($commandKey == "remove") {
            return $cache->remove($cachekey);
        }

        return false;
    }

}