<?php
namespace Ecommerce;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Paginator
 *
 * @author rbiscaino
 */
class Paginator extends \Zend_Paginator_Adapter_Array
{
    
    /** Item count
     *
     * @var integer
     */
    protected $_count = null;
    
    /**
     * Constructor.
     *
     * @param array $array Array to paginate
     */
    public function __construct(array $array, $contador)
    {
        $this->_array = $array;
        $this->_count = $contador;
    }
    
    public function getItems($offset, $itemCountPerPage)
    {
        return $this->_array;
    }
    
    public function setCount($number) 
    {
        $this->_count = $number;
    }
    
}

?>
