<?php
namespace Ecommerce\Model;

use Ecommerce\DAO\PedidoDAO;

/**
 * Description of Pedido
 *
 * @author rodolfo
 */
class Pedido
{
    /**
     * Chave primária
     * @var int 
     */
    private $id;
    
    /**
     * Chave estrangeira de usuário
     * @var int 
     */
    private $user_id;
    
    /**
     * Campo de data de criação
     * @var DateTime 
     */
    private $data_criacao;
    
    /**
     * Campo de data de alteração
     * @var DateTime 
     */
    private $data_alteracao;
    
    /**
     * Getter de chave primária
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }    
    
    /**
     * Getter de chave estrangeira de usuário
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Getter de Descrição
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }
    
    /**
     * Getter de data de criação
     * @return DateTime
     */
    public function getDataCriacao()
    {
        return $this->data_criacao;
    }
    
    /**
     * Getter de data de alteração
     * @return DateTime
     */
    public function getDataAlteracao()
    {
        return $this->data_alteracao;
    }

    /**
     * Setter de chave primária
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    
    /**
     * Setter de chave estrangeira de usuário
     * @param int $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * Setter de Descrição
     * @param string $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * Setter de Data de criação
     * @param DateTime $data_criacao
     */
    public function setDataCriacao($data_criacao)
    {
        $this->data_criacao = $data_criacao;
    }

    /**
     * Setter de Data de alteração
     * @param DateTime $data_alteracao
     */
    public function setDataAlteracao($data_alteracao)
    {
        $this->data_alteracao = $data_alteracao;
    }
    
    /**
     * Método responsável em fazer o persist em banco
     * @return \Ecommerce\Model\Pedido
     */
    public function gravar()
    {
        $pedidoDAO = new PedidoDAO();
        return $pedidoDAO->persist($this);
    }
    
    /**
     * Metodo adiciona Produto ao Pedido
     * @param int $produto_id
     * @param int $qtd
     * @return type
     */
    public function addProduto($produto_id, $qtd)
    {
        $pedidoDAO = new PedidoDAO();
        $pedidoDAO->addProduto($this, $produto_id, $qtd);
    }
    
    /**
     * Retorna os atributos do objeto em array
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }
    
    /**
     * Busca por Categoria
     * @param int $id
     * @return \Ecommerce\Model\Pedido
     */
    static public function findById($id) 
    {
        $pedidoDAO = new PedidoDAO();
        return $pedidoDAO->get($id);
    }
    
    /**
     * Busca Pedidos por usuario id 
     * @param int $user_id
     * @return array
     */
    static public function findByUserId($user_id) 
    {
        $pedidoDAO = new PedidoDAO();
        return $pedidoDAO->getByUserId($user_id);
    }
    
    /**
     * Lista todas as categorias
     * @return array
     */
    static public function listAll()
    {
        $pedidoDAO = new PedidoDAO();
        return $pedidoDAO->getAll();
    }
    
    /**
     * Método de conversão
     * @param stdClass $stdClass
     * @return \Ecommerce\Model\Pedido
     */
    static public function convert($stdClass)
    {
        $pedido = new Pedido();
        $pedido->setId($stdClass->id);
        $pedido->setUserId($stdClass->user_id);
        $pedido->setDataCriacao(\DateTime::createFromFormat('Y-m-d H:i:s', $stdClass->data_criacao));
        $pedido->setDataAlteracao(\DateTime::createFromFormat('Y-m-d H:i:s', $stdClass->data_alteracao));
        
        return $pedido;
    }
    
}
