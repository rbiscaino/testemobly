<?php
namespace Ecommerce\Model;

use Ecommerce\DAO\CategoriaDAO;

/**
 * Description of Categoria
 *
 * @author rodolfo
 */
class Categoria
{
    /**
     * Chave primária
     * @var int 
     */
    private $id;
    
    /**
     * Campo nome
     * @var string 
     */
    private $nome;
    
    /**
     * Campo descrição
     * @var string 
     */
    private $descricao;
    
    /**
     * Campo de data de criação
     * @var DateTime 
     */
    private $data_criacao;
    
    /**
     * Campo de data de alteração
     * @var DateTime 
     */
    private $data_alteracao;
    
    /**
     * Getter de chave primária
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Getter de Nome
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }
    
    /**
     * Getter de Descrição
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }
    
    /**
     * Getter de data de criação
     * @return DateTime
     */
    public function getDataCriacao()
    {
        return $this->data_criacao;
    }
    
    /**
     * Getter de data de alteração
     * @return DateTime
     */
    public function getDataAlteracao()
    {
        return $this->data_alteracao;
    }
    
    /**
     * Setter de chave primária
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = (int)$id;
    }
    
    /**
     * Setter de Nome
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }
    
    /**
     * Setter de Descrição
     * @param string $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }
    
    /**
     * Setter de Data de criação
     * @param DateTime $data_criacao
     */
    public function setDataCriacao($data_criacao)
    {
        $this->data_criacao = $data_criacao;
    }
    
    /**
     * Setter de Data de alteração
     * @param DateTime $data_alteracao
     */
    public function setDataAlteracao($data_alteracao)
    {
        $this->data_alteracao = $data_alteracao;
    }
    
    /**
     * Método responsável em fazer o persist em banco
     * @return \Ecommerce\Model\Categoria
     */
    public function gravar()
    {
        $categoriaDAO = new CategoriaDAO();
        return $categoriaDAO->persist($this);
    }
    
    /**
     * Retorna os atributos do objeto em array
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }
    
    /**
     * Busca por Categoria
     * @param int $id
     * @return \Ecommerce\Model\Categoria
     */
    static public function findById($id) 
    {
        $categoriaDAO = new CategoriaDAO();
        return $categoriaDAO->get($id);
    }
    
    /**
     * Busca de Categoria por Produto id
     * @param int $produto_id
     * @return array
     */
    static public function findByProdutoId($produto_id) 
    {
        $categoriaDAO = new CategoriaDAO();
        return $categoriaDAO->getByProdutoId($produto_id);
    }
    
    /**
     * Lista todas as categorias
     * @return array
     */
    static public function listAll()
    {
        $categoriaDAO = new CategoriaDAO();
        return $categoriaDAO->getAll();
    }
    
    /**
     * Método de conversão
     * @param stdClass $stdClass
     * @return \Ecommerce\Model\Categoria
     */
    static public function convert($stdClass)
    {
        $categoria = new Categoria();
        $categoria->setId($stdClass->id);
        $categoria->setNome($stdClass->nome);
        $categoria->setDescricao($stdClass->descricao);
        $categoria->setDataCriacao(\DateTime::createFromFormat('Y-m-d H:i:s', $stdClass->data_criacao));
        $categoria->setDataAlteracao(\DateTime::createFromFormat('Y-m-d H:i:s', $stdClass->data_alteracao));
        
        return $categoria;
    }
    
}
