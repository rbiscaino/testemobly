<?php
namespace Ecommerce\Model;

use Ecommerce\DAO\UserDAO;

/**
 * Description of User
 *
 * @author rodolfo
 */
class User
{
    /**
     * Chave primária
     * @var int 
     */
    private $id;
    
    /**
     * Chave estrangeira de perfil
     * @var int 
     */
    private $perfil_id;
    
    /**
     * Chave estrangeira de grupo
     * @var int 
     */
    private $grupo_id;
    
    /**
     * Campo nome
     * @var string 
     */
    private $nome;
    
    /**
     * Campo email
     * @var string 
     */
    private $email;
    
    /**
     * Campo password
     * @var string 
     */
    private $password;
    
    /**
     * Campo de data de criação
     * @var DateTime 
     */
    private $data_criacao;
    
    /**
     * Campo de data de alteração
     * @var DateTime 
     */
    private $data_alteracao;
    
    /**
     * Getter de chave primária
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Getter de chave estrangeira de perfil
     * @return int
     */
    public function getPerfilId()
    {
        return $this->perfil_id;
    }

    /**
     * Getter de chave estrangeira de grupo
     * @return int
     */
    public function getGrupoId()
    {
        return $this->grupo_id;
    }

    /**
     * Getter de Nome
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Getter de Email
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Getter de Password
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Getter de data de criação
     * @return DateTime
     */
    public function getDataCriacao()
    {
        return $this->data_criacao;
    }

    /**
     * Getter de data de alteração
     * @return DateTime
     */
    public function getDataAlteracao()
    {
        return $this->data_alteracao;
    }

    /**
     * Setter de chave primária
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Setter de chave estrangeira de perfil
     * @param int $perfil_id
     */
    public function setPerfilId($perfil_id)
    {
        $this->perfil_id = $perfil_id;
    }
    
    /**
     * Setter de chave estrangeira de grupo
     * @param int $grupo_id
     */
    public function setGrupoId($grupo_id)
    {
        $this->grupo_id = $grupo_id;
    }
    
    /**
     * Setter de Nome
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * Setter de Email
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
    
    /**
     * Setter de Password
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Setter de Data de criação
     * @param DateTime $data_criacao
     */
    public function setDataCriacao($data_criacao)
    {
        $this->data_criacao = $data_criacao;
    }

    /**
     * Setter de Data de alteração
     * @param DateTime $data_alteracao
     */
    public function setDataAlteracao($data_alteracao)
    {
        $this->data_alteracao = $data_alteracao;
    }
    
    /**
     * Método responsável em fazer o persist em banco
     * @return \Ecommerce\Model\User
     */
    public function gravar()
    {
        $userDAO = new UserDAO();
        return $userDAO->persist($this);
    }
    
    /**
     * Retorna os atributos do objeto em array
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }
    
    /**
     * Busca por User
     * @param int $id
     * @return \Ecommerce\Model\User
     */
    static public function findById($id) 
    {
        $userDAO = new UserDAO();
        return $userDAO->get($id);
    }
    
    /**
     * Lista todas as categorias
     * @return array
     */
    static public function listAll()
    {
        $userDAO = new UserDAO();
        return $userDAO->getAll();
    }
    
    /**
     * Busca usuário por email e password
     * @param string $email
     * @param string $password
     * @return null | \Ecommerce\Model\User
     */
    static public function findByEmailByPassword($email, $password)
    {
        if(empty($email) || empty($password))
        {
            return null;
        }
        
        $userDAO = new UserDAO();
        return $userDAO->getByEmailByPassword($email, $password);
    }    
    
    /**
     * Método de conversão
     * @param stdClass $stdClass
     * @return \Ecommerce\Model\Categoria
     */
    static public function convert($stdClass)
    {
        $user = new User();
        $user->setId($stdClass->id);
        $user->setPerfilId($stdClass->perfil_id);
        $user->setGrupoId($stdClass->grupo_id);
        $user->setNome($stdClass->nome);
        $user->setEmail($stdClass->email);
        $user->setPassword($stdClass->password);
        $user->setDataCriacao(\DateTime::createFromFormat('Y-m-d H:i:s', $stdClass->data_criacao));
        $user->setDataAlteracao(\DateTime::createFromFormat('Y-m-d H:i:s', $stdClass->data_alteracao));
        
        return $user;
    }
    
}
