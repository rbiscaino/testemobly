<?php
namespace Ecommerce\Model;

use Ecommerce\DAO\GrupoDAO;

/**
 * Description of Grupo
 *
 * @author rodolfo
 */
class Grupo
{
    /**
     * Chave primária
     * @var int 
     */
    private $id;
    
    /**
     * Campo Nome
     * @var string 
     */
    private $nome;
    
    
    /**
     * Getter de chave primária
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Getter de Nome
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }
    
    /**
     * Setter de chave primária
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = (int)$id;
    }
    
    /**
     * Setter de Nome
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }
    
    /**
     * Método responsável em fazer o persist em banco
     * @return \Ecommerce\Model\Grupo
     */
    public function gravar()
    {
        $grupoDAO = new GrupoDAO();
        return $grupoDAO->persist($this);
    }
    
    /**
     * Retorna os atributos do objeto em array
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }
    
    /**
     * Busca por Grupo
     * @param int $id
     * @return \Ecommerce\Model\Grupo
     */
    static public function findById($id) 
    {
        $grupoDAO = new GrupoDAO();
        return $grupoDAO->get($id);
    }

    /**
     * Método de conversão
     * @param stdClass $stdClass
     * @return \Ecommerce\Model\Grupo
     */
    static public function convert($stdClass)
    {
        $grupo = new Grupo();
        $grupo->setId($stdClass->id);
        $grupo->setNome($stdClass->nome);
        
        return $grupo;
    }
    
}
