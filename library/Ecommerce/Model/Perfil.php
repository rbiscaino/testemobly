<?php
namespace Ecommerce\Model;

use Ecommerce\DAO\PerfilDAO;

/**
 * Description of Perfil
 *
 * @author rodolfo
 */
class Perfil
{
    /**
     * Chave primária
     * @var int 
     */
    private $id;
    
    /**
     * Logradouro
     * @var string 
     */
    private $logradouro;
    
    /**
     * Número
     * @var string 
     */
    private $numero;
    
    /**
     * Complemento
     * @var string 
     */
    private $complemento;
    
    /**
     * CEP
     * @var string 
     */
    private $cep;
    
    /**
     * Chave para cidade id
     * @var int 
     */
    private $cidade_id;
    
    /**
     * Campo de data de criação
     * @var DateTime 
     */
    private $data_criacao;
    
    /**
     * Campo de data de alteração
     * @var DateTime 
     */
    private $data_alteracao;
    
    /**
     * Getter de chave primária
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Getter de Logradouro
     * @return string
     */
    public function getLogradouro()
    {
        return $this->logradouro;
    }

    /**
     * Getter de Número
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Getter de Complemento
     * @return string
     */
    public function getComplemento()
    {
        return $this->complemento;
    }

    /**
     * Getter de CEP
     * @return string
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * Getter de cidade id
     * @return int
     */
    public function getCidadeId()
    {
        return $this->cidade_id;
    }
    
    /**
     * Getter de data de criação
     * @return DateTime
     */
    public function getDataCriacao()
    {
        return $this->data_criacao;
    }

    /**
     * Getter de data de alteração
     * @return DateTime
     */
    public function getDataAlteracao()
    {
        return $this->data_alteracao;
    }

    /**
     * Setter de chave primária
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Setter de Logradouro
     * @param string $logradouro
     */
    public function setLogradouro($logradouro)
    {
        $this->logradouro = $logradouro;
    }

    /**
     * Setter de Número
     * @param string $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * Setter de complemento
     * @param string $complemento
     */
    public function setComplemento($complemento)
    {
        $this->complemento = $complemento;
    }

    /**
     * Setter de CEP
     * @param string $cep
     */
    public function setCep($cep)
    {
        $this->cep = $cep;
    }

    /**
     * Setter de cidade id
     * @param int $cidade_id
     */
    public function setCidadeId($cidade_id)
    {
        $this->cidade_id = $cidade_id;
    }
    
    /**
     * Setter de Data de criação
     * @param DateTime $data_criacao
     */
    public function setDataCriacao($data_criacao)
    {
        $this->data_criacao = $data_criacao;
    }

    /**
     * Setter de Data de alteração
     * @param DateTime $data_alteracao
     */
    public function setDataAlteracao($data_alteracao)
    {
        $this->data_alteracao = $data_alteracao;
    }
    
    /**
     * Método responsável em fazer o persist em banco
     * @return \Ecommerce\Model\Perfil
     */
    public function gravar()
    {
        $perfilDAO = new PerfilDAO();
        return $perfilDAO->persist($this);
    }
    
    /**
     * Retorna os atributos do objeto em array
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }
    
    /**
     * Busca por Perfil
     * @param int $id
     * @return \Ecommerce\Model\Perfil
     */
    static public function findById($id) 
    {
        $perfilDAO = new PerfilDAO();
        return $perfilDAO->get($id);
    }
    
    /**
     * Método de conversão
     * @param stdClass $stdClass
     * @return \Ecommerce\Model\Categoria
     */
    static public function convert($stdClass)
    {
        $perfil = new Perfil();
        $perfil->setId($stdClass->id);
        $perfil->setLogradouro($stdClass->logradouro);
        $perfil->setNumero($stdClass->numero);
        $perfil->setComplemento($stdClass->complemento);
        $perfil->setCep($stdClass->cep);
        $perfil->setCidadeId($stdClass->cidade_id);
        $perfil->setDataCriacao(\DateTime::createFromFormat('Y-m-d H:i:s', $stdClass->data_criacao));
        $perfil->setDataAlteracao(\DateTime::createFromFormat('Y-m-d H:i:s', $stdClass->data_alteracao));
        
        return $perfil;
    }
    
}
