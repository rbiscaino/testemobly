<?php
namespace Ecommerce\Model;

use Ecommerce\DAO\ProdutoDAO;

/**
 * Description of Produto
 *
 * @author rodolfo
 */
class Produto
{
    /**
     * Chave primária
     * @var int 
     */
    private $id;
    
    /**
     * Campo nome
     * @var string 
     */
    private $nome;
    
    /**
     * Campo descrição
     * @var string 
     */
    private $descricao;
    
    /**
     * Campo caminho imagem
     * @var string 
     */
    private $imagem;
    
    /**
     * Campo preço
     * @var string 
     */
    private $preco;
    
    /**
     * Campo de data de criação
     * @var DateTime 
     */
    private $data_criacao;
    
    /**
     * Campo de data de alteração
     * @var DateTime 
     */
    private $data_alteracao;
    
    
    /**
     * Getter de chave primária
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Getter de Nome
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Getter de Descrição
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }
    
    /**
     * Getter de caminho da imagem
     * @return string
     */
    public function getImagem()
    {
        return $this->imagem;
    }

    /**
     * Getter de Preço
     * @return string
     */
    public function getPreco()
    {
        return $this->preco;
    }

    /**
     * Getter de data de criação
     * @return DateTime
     */
    public function getDataCriacao()
    {
        return $this->data_criacao;
    }

    /**
     * Getter de data de alteração
     * @return DateTime
     */
    public function getDataAlteracao()
    {
        return $this->data_alteracao;
    }

    /**
     * Setter de chave primária
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = (int)$id;
    }

    /**
     * Setter de Nome
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * Setter de Descrição
     * @param string $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }
    
    /**
     * Setter de caminho da imagem
     * @param string $imagem
     */
    public function setImagem($imagem)
    {
        $this->imagem = $imagem;
    }
    
    /**
     * Setter de Preço
     * @param string $preco
     */
    public function setPreco($preco)
    {
        $this->preco = (double)$preco;
    }

    /**
     * Setter de Data de criação
     * @param DateTime $data_criacao
     */
    public function setDataCriacao($data_criacao)
    {
        $this->data_criacao = $data_criacao;
    }
    
    /**
     * Setter de Data de alteração
     * @param DateTime $data_alteracao
     */
    public function setDataAlteracao($data_alteracao)
    {
        $this->data_alteracao = $data_alteracao;
    }
    
    /**
     * Gera uma chave para o CACHE
     * @return string
     */
    public function gerarChaveCache()
    {
        $chave = 'ecommerce_produto_'. $this->getId();
        return $chave;
    }
    
    /**
     * Gera uma chave para o CACHE
     * @param int $categoria_id
     * @return string
     */
    public static function gerarChaveCacheCategoria($categoria_id)
    {
        $chave = 'ecommerce_produto_categoria'. $categoria_id;
        return $chave;
    }
    
    /**
     * Método responsável em fazer o persist em banco
     * @return \Ecommerce\Model\Produto
     */
    public function gravar()
    {
        $produtoDAO = new ProdutoDAO();
        return $produtoDAO->persist($this);
    }
    
    /**
     * Adiciona categorias para o Produto
     * @param int $categoria_id
     */
    public function addCategoria($categoria_id)
    {
        $produtoDAO = new ProdutoDAO();
        $produtoDAO->addCategoria($this, $categoria_id);
        
        $arr_produtos = \Ecommerce\MemcacheMutex::mutexAction('load', self::gerarChaveCacheCategoria($categoria_id));
        
        if($arr_produtos) {
            $arr_produtos[] = $this;
            \Ecommerce\MemcacheMutex::mutexAction('save', self::gerarChaveCacheCategoria($categoria_id), $arr_produtos);
        } else {
            $arr_produtos = array();
            $arr_produtos[] = $this;
            \Ecommerce\MemcacheMutex::mutexAction('save', self::gerarChaveCacheCategoria($categoria_id), $arr_produtos);
        }
        
    }
    
    
    /**
     * Realiza uma busca com paginação
     * @param array $arr_parametros
     * @return array
     */
    public static function getProdutos($arr_parametros = array())
    {
        $produtoDAO = new ProdutoDAO();
        return $produtoDAO->getProdutos($arr_parametros);
    }
    
    /**
     * Remove todas as categorias vinculada ao produto
     */
    public function removerCategorias()
    {
        $produtoDAO = new ProdutoDAO();
        $produtoDAO->removeCategorias($this);
    }
    
    /**
     * Retorna os atributos do objeto em array
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }
    
    /**
     * Busca por Categoria
     * @param int $id
     * @return \Ecommerce\Model\Produto
     */
    static public function findById($id) 
    {
        $produtoDAO = new ProdutoDAO();
        return $produtoDAO->get($id);
    }
    
    /**
     * Busca de Produto por categoria id
     * @param int $categoria_id
     * @return array
     */
    static public function findByCategoriaId($categoria_id)
    {
        $arr_produtos = \Ecommerce\MemcacheMutex::mutexAction('load', self::gerarChaveCacheCategoria($categoria_id));
        if($arr_produtos) {
            return $arr_produtos;
        } else {
            $produtoDAO = new ProdutoDAO();
            $arr_produtos = $produtoDAO->getByCategoriaId($categoria_id);
            \Ecommerce\MemcacheMutex::mutexAction('save', self::gerarChaveCacheCategoria($categoria_id), $arr_produtos);
            return $arr_produtos;
        }
    }
    
    /**
     * Busca de Produto por pedido id
     * @param int $pedido_id
     * @return array
     */
    static public function findByPedidoId($pedido_id)
    {
        $produtoDAO = new ProdutoDAO();
        return $produtoDAO->getByPedidoId($pedido_id);
    }
    
    /**
     * Busca de PedidoProduto por pedido id
     * @param int $pedido_id
     * @return array
     */
    static public function findPedidoProdutoByPedidoId($pedido_id)
    {
        $produtoDAO = new ProdutoDAO();
        return $produtoDAO->getPedidoProdutoByPedidoId($pedido_id);
    }
    
    /**
     * Lista todas as categorias
     * @return array
     */
    static public function listAll()
    {
        $produtoDAO = new ProdutoDAO();
        return $produtoDAO->getAll();
    }
    
    /**
     * Método de conversão
     * @param stdClass $stdClass
     * @return \Ecommerce\Model\Produto
     */
    static public function convert($stdClass)
    {
        $produto = new Produto();
        $produto->setId($stdClass->id);
        $produto->setNome($stdClass->nome);
        $produto->setDescricao($stdClass->descricao);
        $produto->setImagem($stdClass->imagem);
        $produto->setPreco($stdClass->preco);
        $produto->setDataCriacao(\DateTime::createFromFormat('Y-m-d H:i:s', $stdClass->data_criacao));
        $produto->setDataAlteracao(\DateTime::createFromFormat('Y-m-d H:i:s', $stdClass->data_alteracao));
        
        return $produto;
    }
    
}
