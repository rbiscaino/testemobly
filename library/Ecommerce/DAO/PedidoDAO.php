<?php

namespace Ecommerce\DAO;

use Ecommerce\DBMysql;
use Ecommerce\Model\Pedido;

/**
 * Description of PedidoDAO
 *
 * @author rodolfo
 */
class PedidoDAO
{
    private $_db;
    private $_table_name = 'pedido';
    
    public function __construct()
    {
        $front = \Zend_Controller_Front::getInstance();
        $this->_db = $front->getParam('bootstrap')->getContainer()->database;
    }
    
    public function get($id)
    {
        $sqlStr = "SELECT * FROM " . $this->_table_name . " WHERE id = " . DBMysql::SQLValue($id, false, 'int');

        if ($pedidoEntity = $this->_db->QuerySingleRow($sqlStr)) {
            return Pedido::convert($pedidoEntity);
        } else {
            return null;
        }
    }
    
    public function getByUserId($user_id)
    {
        $sqlStr = "SELECT * FROM " . $this->_table_name . " WHERE user_id = ". DBMysql::SQLValue($user_id, false, 'int');
        
        $this->_db->Query($sqlStr);
        
        $pedidosEntity = $this->_db->RecordsObject();
        
        $arr_pedido = array();
        if($pedidosEntity) {
            foreach($pedidosEntity as $pedidoEntity) {
                $arr_pedido[] = Pedido::convert($pedidoEntity);
            }            
        }
        
        return $arr_pedido;
    }
    
    public function getAll()
    {
        $sqlStr = "SELECT * FROM " . $this->_table_name;
        
        $this->_db->Query($sqlStr);
        
        $pedidosEntity = $this->_db->RecordsObject();
        
        $arr_pedido = array();
        if($pedidosEntity) {
            foreach($pedidosEntity as $pedidoEntity) {
                $arr_pedido[] = Pedido::convert($pedidoEntity);
            }            
        }
        
        return $arr_pedido;
    }
    
    public function persist(Pedido $pedido)
    {
        if ($pedido->getId()) {
            $sqlStr = "UPDATE " . $this->_table_name . " SET
                                    user_id = " . DBMysql::SQLValue($pedido->getUserId()) . " ,
                                    data_criacao = " . DBMysql::SQLValue($pedido->getDataCriacao()) . ",
                                    data_alteracao = " . DBMysql::SQLValue(new \DateTime('now')) . "
                               WHERE
                                    id = " . DBMysql::SQLValue($pedido->getId());
        } else {
            $sqlStr = "INSERT INTO " . $this->_table_name . " 
                                    (
                                        id,
                                        user_id,
                                        data_criacao,
                                        data_alteracao
                                    )
                              VALUES 
                                    (
                                        NULL,
                                        " . DBMysql::SQLValue($pedido->getUserId()) . ",
                                        " . DBMysql::SQLValue(new \DateTime('now')) . ",
                                        " . DBMysql::SQLValue(new \DateTime('now')) . "
                                    )";
        }

        $this->_db->Query($sqlStr);

        $pedido_id = ($pedido->getId() ? $pedido->getId() : $this->_db->GetLastInsertID() );
        $pedido->setId($pedido_id);

        return $pedido;
    }
    
    public function addProduto(Pedido $pedido, $produto_id, $qtd)
    {
        $sqlStr = "INSERT INTO pedido_produto 
                                    (
                                        id,
                                        pedido_id,
                                        produto_id,
                                        quantidade,
                                        data_criacao,
                                        data_alteracao
                                    )
                              VALUES 
                                    (
                                        NULL,
                                        " . DBMysql::SQLValue($pedido->getId(), false, 'int') . ",
                                        " . DBMysql::SQLValue($produto_id, false, 'int') . ",
                                        " . DBMysql::SQLValue($qtd) . ",
                                        " . DBMysql::SQLValue(new \DateTime('now')) . ",
                                        " . DBMysql::SQLValue(new \DateTime('now')) . "
                                    )";
        $this->_db->Query($sqlStr);
    }
}
