<?php

namespace Ecommerce\DAO;

use Ecommerce\DBMysql;
use Ecommerce\Model\Produto;

/**
 * Description of ProdutoDAO
 *
 * @author rodolfo
 */
class ProdutoDAO
{
    private $_db;
    private $_table_name = 'produto';
    
    public function __construct()
    {
        $front = \Zend_Controller_Front::getInstance();
        $this->_db = $front->getParam('bootstrap')->getContainer()->database;
    }
    
    public function get($id)
    {
        $sqlStr = "SELECT * FROM " . $this->_table_name . " WHERE id = " . DBMysql::SQLValue($id, false, 'int');

        if ($produtoEntity = $this->_db->QuerySingleRow($sqlStr)) {
            return Produto::convert($produtoEntity);
        } else {
            return null;
        }
    }
    
    public function getAll()
    {
        $sqlStr = "SELECT * FROM " . $this->_table_name;
        
        $this->_db->Query($sqlStr);
        
        $produtosEntity = $this->_db->RecordsObject();
        
        $arr_produto = array();
        if($produtosEntity) {
            foreach($produtosEntity as $produtoEntity) {
                $arr_produto[] = Produto::convert($produtoEntity);
            }            
        }
        
        return $arr_produto;
    }
    
    public function persist(Produto $produto)
    {
        if ($produto->getId()) {
            $sqlStr = "UPDATE " . $this->_table_name . " SET
                                    nome = " . DBMysql::SQLValue($produto->getNome()) . " ,
                                    descricao = " . DBMysql::SQLValue($produto->getDescricao()) . " ,
                                    imagem = " . DBMysql::SQLValue($produto->getImagem()) . " ,
                                    preco = " . DBMysql::SQLValue($produto->getPreco()) . " ,
                                    data_criacao = " . DBMysql::SQLValue($produto->getDataCriacao()) . ",
                                    data_alteracao = " . DBMysql::SQLValue(new \DateTime('now')) . "
                               WHERE
                                    id = " . DBMysql::SQLValue($produto->getId());
        } else {
            $sqlStr = "INSERT INTO " . $this->_table_name . " 
                                    (
                                        id,
                                        nome,
                                        descricao,
                                        imagem,
                                        preco,
                                        data_criacao,
                                        data_alteracao
                                    )
                              VALUES 
                                    (
                                        NULL,
                                        " . DBMysql::SQLValue($produto->getNome()) . ",
                                        " . DBMysql::SQLValue($produto->getDescricao()) . ",
                                        " . DBMysql::SQLValue($produto->getImagem()) . ",
                                        " . DBMysql::SQLValue($produto->getPreco()) . ",
                                        " . DBMysql::SQLValue(new \DateTime('now')) . ",
                                        " . DBMysql::SQLValue(new \DateTime('now')) . "
                                    )";
        }

        $this->_db->Query($sqlStr);

        $produto_id = ($produto->getId() ? $produto->getId() : $this->_db->GetLastInsertID() );
        $produto->setId($produto_id);

        return $produto;
    }
    
    public function getByCategoriaId($categoria_id)
    {
        $sqlStr = "SELECT "
                . "     p.* "
                . " FROM "
                . "     " . $this->_table_name . " AS p"
                . "     INNER JOIN categoria_produto AS cp ON cp.produto_id = p.id"
                . " WHERE"
                . "     cp.categoria_id = ". DBMysql::SQLValue($categoria_id, false, 'int');
        
        $this->_db->Query($sqlStr);
        
        $produtosEntity = $this->_db->RecordsObject();
        
        $arr_produto = array();
        if($produtosEntity) {
            foreach($produtosEntity as $produtoEntity) {
                $arr_produto[] = Produto::convert($produtoEntity);
            }            
        }
        
        return $arr_produto;
    }
    
    public function getByPedidoId($pedido_id)
    {
        $sqlStr = "SELECT "
                . "     p.* "
                . " FROM "
                . "     " . $this->_table_name . " AS p"
                . "     INNER JOIN pedido_produto AS cp ON cp.produto_id = p.id"
                . " WHERE"
                . "     cp.pedido_id = ". DBMysql::SQLValue($pedido_id, false, 'int');
        
        $this->_db->Query($sqlStr);
        
        $produtosEntity = $this->_db->RecordsObject();
        
        $arr_produto = array();
        if($produtosEntity) {
            foreach($produtosEntity as $produtoEntity) {
                $arr_produto[] = Produto::convert($produtoEntity);
            }            
        }
        
        return $arr_produto;
    }
    
    public function getPedidoProdutoByPedidoId($pedido_id)
    {
        $sqlStr = "SELECT "
                . "     cp.*, p.nome, p.preco "
                . " FROM "
                . "     " . $this->_table_name . " AS p"
                . "     INNER JOIN pedido_produto AS cp ON cp.produto_id = p.id"
                . " WHERE"
                . "     cp.pedido_id = ". DBMysql::SQLValue($pedido_id, false, 'int');
        
        $this->_db->Query($sqlStr);
        
        $produtosEntity = $this->_db->RecordsObject();
        
        $arr_produto = array();
        if($produtosEntity) {
            foreach($produtosEntity as $produtoEntity) {
                $arr_produto[] = $produtoEntity;
            }            
        }
        
        return $arr_produto;
    }
    
    public function getProdutos($arr_parametros = array()) 
    {
        $sqlStr = "SELECT "
                . "     * "
                . "FROM "
                .       $this->_table_name;
        
        if($arr_parametros['nome']) {
            $sqlStr .= " WHERE nome LIKE ". DBMysql::SQLValue("%".$arr_parametros['nome']."%") . " OR descricao LIKE ". DBMysql::SQLValue("%".$arr_parametros['nome']."%");
        }
        
        $sqlLimit = \Ecommerce\PaginatorBuilder::gerarSqlLimit($arr_parametros);
        $sql = $sqlStr . $sqlLimit;
        
        $this->_db->Query($sql);
        $ret = $this->_db->RecordsObject();
        
        $produtos = array();
        if ($ret) {
            foreach ($ret as $produto) {
                $produtos[] = \Ecommerce\Model\Produto::convert($produto);
            }            
        }        

        return \Ecommerce\PaginatorBuilder::paginarResultadosQuery($sql, $produtos, $arr_parametros);
        
    }
    
    public function addCategoria(Produto $produto, $categoria_id)
    {
        $sqlStr = "INSERT INTO categoria_produto 
                                    (
                                        id,
                                        produto_id,
                                        categoria_id,
                                        data_criacao,
                                        data_alteracao
                                    )
                              VALUES 
                                    (
                                        NULL,
                                        " . DBMysql::SQLValue($produto->getId()) . ",
                                        " . DBMysql::SQLValue($categoria_id) . ",
                                        " . DBMysql::SQLValue(new \DateTime('now')) . ",
                                        " . DBMysql::SQLValue(new \DateTime('now')) . "
                                    )";
        $this->_db->Query($sqlStr);
    }
    
    public function removeCategorias(Produto $produto)
    {
        $sqlStr = "DELETE FROM categoria_produto WHERE produto_id = ". DBMysql::SQLValue($produto->getId(), false, 'int');
        $this->_db->Query($sqlStr);
    }
}
