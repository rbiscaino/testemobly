<?php

namespace Ecommerce\DAO;

use Ecommerce\DBMysql;
use Ecommerce\Model\Grupo;

/**
 * Description of GrupoDAO
 *
 * @author rodolfo
 */
class GrupoDAO
{
    private $_db;
    private $_table_name = 'grupo';
    
    public function __construct()
    {
        $front = \Zend_Controller_Front::getInstance();
        $this->_db = $front->getParam('bootstrap')->getContainer()->database;
    }
    
    public function get($id)
    {
        $sqlStr = "SELECT * FROM " . $this->_table_name . " WHERE id = " . DBMysql::SQLValue($id, false, 'int');

        if ($grupoEntity = $this->_db->QuerySingleRow($sqlStr)) {
            return Grupo::convert($grupoEntity);
        } else {
            return null;
        }
    }
    
    public function persist(Grupo $grupo)
    {
        if ($grupo->getId()) {
            $sqlStr = "UPDATE " . $this->_table_name . " SET
                                    nome = " . DBMysql::SQLValue($grupo->getNome()) . " 
                               WHERE
                                    id = " . DBMysql::SQLValue($grupo->getId());
        } else {
            $sqlStr = "INSERT INTO " . $this->_table_name . " 
                                    (
                                        id,
                                        nome
                                    )
                              VALUES 
                                    (
                                        NULL,
                                        " . DBMysql::SQLValue($grupo->getNome()) . "
                                    )";
        }

        $this->_db->Query($sqlStr);

        $grupo_id = ($grupo->getId() ? $grupo->getId() : $this->_db->GetLastInsertID() );
        $grupo->setId($grupo_id);

        return $grupo;
    }
}
