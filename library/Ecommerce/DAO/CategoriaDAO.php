<?php

namespace Ecommerce\DAO;

use Ecommerce\DBMysql;
use Ecommerce\Model\Categoria;

/**
 * Description of CategoriaDAO
 *
 * @author rodolfo
 */
class CategoriaDAO
{
    private $_db;
    private $_table_name = 'categoria';
    
    public function __construct()
    {
        $front = \Zend_Controller_Front::getInstance();
        $this->_db = $front->getParam('bootstrap')->getContainer()->database;
    }
    
    public function get($id)
    {
        $sqlStr = "SELECT * FROM " . $this->_table_name . " WHERE id = " . DBMysql::SQLValue($id, false, 'int');

        if ($categoriaEntity = $this->_db->QuerySingleRow($sqlStr)) {
            return Categoria::convert($categoriaEntity);
        } else {
            return null;
        }
    }
    
    public function getByProdutoId($produto_id)
    {
        $sqlStr = "SELECT "
                . "     c.* "
                . "FROM " . $this->_table_name. " AS c "
                . "     INNER JOIN categoria_produto AS cp ON c.id = cp.categoria_id "
                . "WHERE "
                . "     cp.produto_id = ". DBMysql::SQLValue($produto_id, false, 'int');
        
        $this->_db->Query($sqlStr);
        
        $categoriasEntity = $this->_db->RecordsObject();
        
        $arr_caregoria = array();
        if($categoriasEntity) {
            foreach($categoriasEntity as $categoriaEntity) {
                $arr_caregoria[] = Categoria::convert($categoriaEntity);
            }            
        }
        
        return $arr_caregoria;
    }
    
    public function getAll()
    {
        $sqlStr = "SELECT * FROM " . $this->_table_name;
        
        $this->_db->Query($sqlStr);
        
        $categoriasEntity = $this->_db->RecordsObject();
        
        $arr_caregoria = array();
        if($categoriasEntity) {
            foreach($categoriasEntity as $categoriaEntity) {
                $arr_caregoria[] = Categoria::convert($categoriaEntity);
            }            
        }
        
        return $arr_caregoria;
    }
    
    public function persist(Categoria $categoria)
    {
        if ($categoria->getId()) {
            $sqlStr = "UPDATE " . $this->_table_name . " SET
                                    nome = " . DBMysql::SQLValue($categoria->getNome()) . " ,
                                    descricao = " . DBMysql::SQLValue($categoria->getDescricao()) . " ,
                                    data_criacao = " . DBMysql::SQLValue($categoria->getDataCriacao()) . ",
                                    data_alteracao = " . DBMysql::SQLValue(new \DateTime('now')) . "
                               WHERE
                                    id = " . DBMysql::SQLValue($categoria->getId());
        } else {
            $sqlStr = "INSERT INTO " . $this->_table_name . " 
                                    (
                                        id,
                                        nome,
                                        descricao,
                                        data_criacao,
                                        data_alteracao
                                    )
                              VALUES 
                                    (
                                        NULL,
                                        " . DBMysql::SQLValue($categoria->getNome()) . ",
                                        " . DBMysql::SQLValue($categoria->getDescricao()) . ",
                                        " . DBMysql::SQLValue(new \DateTime('now')) . ",
                                        " . DBMysql::SQLValue(new \DateTime('now')) . "
                                    )";
        }

        $this->_db->Query($sqlStr);

        $categoria_id = ($categoria->getId() ? $categoria->getId() : $this->_db->GetLastInsertID() );
        $categoria->setId($categoria_id);

        return $categoria;
    }
}
