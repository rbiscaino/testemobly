<?php

namespace Ecommerce\DAO;

use Ecommerce\DBMysql;
use Ecommerce\Model\Perfil;

/**
 * Description of PerfilDAO
 *
 * @author rodolfo
 */
class PerfilDAO
{
    private $_db;
    private $_table_name = 'perfil';
    
    public function __construct()
    {
        $front = \Zend_Controller_Front::getInstance();
        $this->_db = $front->getParam('bootstrap')->getContainer()->database;
    }
    
    public function get($id)
    {
        $sqlStr = "SELECT * FROM " . $this->_table_name . " WHERE id = " . DBMysql::SQLValue($id, false, 'int');

        if ($perfilEntity = $this->_db->QuerySingleRow($sqlStr)) {
            return Perfil::convert($perfilEntity);
        } else {
            return null;
        }
    }
    
    public function persist(Perfil $perfil)
    {
        if ($perfil->getId()) {
            $sqlStr = "UPDATE " . $this->_table_name . " SET
                                    logradouro = " . DBMysql::SQLValue($perfil->getLogradouro()) . ",
                                    numero = " . DBMysql::SQLValue($perfil->getNumero()) . ",
                                    complemento = " . DBMysql::SQLValue($perfil->getComplemento()) . " ,
                                    cep = " . DBMysql::SQLValue($perfil->getCep()) . " ,
                                    cidade_id = " . DBMysql::SQLValue($perfil->getCidadeId()) . " ,
                                    data_criacao = " . DBMysql::SQLValue($perfil->getDataCriacao()) . ",
                                    data_alteracao = " . DBMysql::SQLValue(new \DateTime('now')) . "
                               WHERE
                                    id = " . DBMysql::SQLValue($perfil->getId());
        } else {
            $sqlStr = "INSERT INTO " . $this->_table_name . " 
                                    (
                                        id,
                                        logradouro,
                                        numero,
                                        complemento,
                                        cep,
                                        cidade_id,
                                        data_criacao,
                                        data_alteracao
                                    )
                              VALUES 
                                    (
                                        NULL,
                                        " . DBMysql::SQLValue($perfil->getLogradouro()) . ",
                                        " . DBMysql::SQLValue($perfil->getNumero()) . ",
                                        " . DBMysql::SQLValue($perfil->getComplemento()) . ",
                                        " . DBMysql::SQLValue($perfil->getCep()) . ",
                                        " . DBMysql::SQLValue($perfil->getCidadeId()) . ",
                                        " . DBMysql::SQLValue(new \DateTime('now')) . ",
                                        " . DBMysql::SQLValue(new \DateTime('now')) . "
                                    )";
        }

        $this->_db->Query($sqlStr);

        $perfil_id = ($perfil->getId() ? $perfil->getId() : $this->_db->GetLastInsertID() );
        $perfil->setId($perfil_id);

        return $perfil;
    }
}
