<?php

namespace Ecommerce\DAO;

use Ecommerce\DBMysql;
use Ecommerce\Model\User;

/**
 * Description of UserDAO
 *
 * @author rodolfo
 */
class UserDAO
{
    private $_db;
    private $_table_name = 'user';
    
    public function __construct()
    {
        $front = \Zend_Controller_Front::getInstance();
        $this->_db = $front->getParam('bootstrap')->getContainer()->database;
    }
    
    public function get($id)
    {
        $sqlStr = "SELECT * FROM " . $this->_table_name . " WHERE id = " . DBMysql::SQLValue($id, false, 'int');

        if ($userEntity = $this->_db->QuerySingleRow($sqlStr)) {
            return User::convert($userEntity);
        } else {
            return null;
        }
    }
    
    public function getAll()
    {
        $sqlStr = "SELECT * FROM " . $this->_table_name;
        
        $this->_db->Query($sqlStr);
        
        $usersEntity = $this->_db->RecordsObject();
        
        $arr_users = array();
        if($usersEntity) {
            foreach($usersEntity as $userEntity) {
                $arr_users[] = User::convert($userEntity);
            }            
        }
        
        return $arr_users;
    }
    
    public function getByEmailByPassword($email, $password)
    {
        $sqlStr = "SELECT * FROM " . $this->_table_name . " WHERE email = ". DBMysql::SQLValue($email) . " AND password = ". DBMysql::SQLValue(md5($password));
        
        if ($userEntity = $this->_db->QuerySingleRow($sqlStr)) {
            return User::convert($userEntity);
        } else {
            return null;
        }
        
    }
    
    public function persist(User $user)
    {
        if ($user->getId()) {
            $sqlStr = "UPDATE " . $this->_table_name . " SET
                                    perfil_id = " . DBMysql::SQLValue(( $user->getPerfilId() ? $user->getPerfilId() : NULL)) . ",
                                    grupo_id = " . DBMysql::SQLValue(( $user->getGrupoId() ? $user->getGrupoId() : NULL)) . ",
                                    nome = " . DBMysql::SQLValue($user->getNome()) . " ,
                                    email = " . DBMysql::SQLValue($user->getEmail()) . " ,
                                    password = " . DBMysql::SQLValue($user->getPassword()) . " ,
                                    data_criacao = " . DBMysql::SQLValue($user->getDataCriacao()) . ",
                                    data_alteracao = " . DBMysql::SQLValue(new \DateTime('now')) . "
                               WHERE
                                    id = " . DBMysql::SQLValue($user->getId());
        } else {
            $sqlStr = "INSERT INTO " . $this->_table_name . " 
                                    (
                                        id,
                                        perfil_id,
                                        grupo_id,
                                        nome,
                                        email,
                                        password,
                                        data_criacao,
                                        data_alteracao
                                    )
                              VALUES 
                                    (
                                        NULL,
                                        " . DBMysql::SQLValue(( $user->getPerfilId() ? $user->getPerfilId() : NULL)) . ",
                                        " . DBMysql::SQLValue(( $user->getGrupoId() ? $user->getGrupoId() : NULL)) . ",
                                        " . DBMysql::SQLValue($user->getNome()) . ",
                                        " . DBMysql::SQLValue($user->getEmail()) . ",
                                        " . DBMysql::SQLValue($user->getPassword()) . ",
                                        " . DBMysql::SQLValue(new \DateTime('now')) . ",
                                        " . DBMysql::SQLValue(new \DateTime('now')) . "
                                    )";
        }

        $this->_db->Query($sqlStr);

        $user_id = ($user->getId() ? $user->getId() : $this->_db->GetLastInsertID() );
        $user->setId($user_id);

        return $user;
    }
}
