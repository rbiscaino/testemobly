<?php

namespace Ecommerce;

/**
 * Description of Auth
 *
 * @author rodolfo
 */
class Auth
{
    
    public static function autenticar($email, $senha)
    {
        if($user = \Ecommerce\Model\User::findByEmailByPassword($email, $senha)) {
            $auth = \Zend_Auth::getInstance();
            $auth->getStorage()->write($user);
            $credential = new \Zend_Session_Namespace('credential');
            $credential->credential = $user;

            return $user;
        } else {
            return null;
        }
    }
    
}
