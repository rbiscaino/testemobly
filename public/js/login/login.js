$(document).ready(function(){
   $("#btn_sigin").click(function(){
      $.ajax({
        type: 'post',
        url: "/site/login",
        data:{
            email     : $("#email").val(),
            password  : $("#password").val()
        },
        success: function(data){
            if(data.success == true) {
                $("#form-login").hide();
                $("#name-user").html(data.user.nome);
                $("#bem-vindo").show();
            }
        },
        error: function(requisicao){
            if(window.console) console.log('Erro ao atualizar: '+requisicao.status+' - '+requisicao.statusText)
        }
    }); 
   });
});


