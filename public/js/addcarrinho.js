$(document).ready(function(){
    $(".botao-carrinho").click(function(){
        
        produto_id = $(this).attr('id').replace("botao-produto-", "");
        qtd = $("#produto-"+ produto_id).val();
        
        $.ajax({
            type: 'post',
            url: "/site/index/carrinho",
            data:{
                produtoid : produto_id,
                qtd : qtd
            },
            success: function(data){
                if(data.success == true) {
                    alert(data.message);
                }
            },
            error: function(requisicao){
                if(window.console) console.log('Erro ao atualizar: '+requisicao.status+' - '+requisicao.statusText)
            }
        }); 
    });
});