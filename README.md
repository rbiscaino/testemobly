testemobly
==========

Teste a pedido da Mobly

Passos para instalação
======================

Sistema foi criado em cima da configuração: PHP5.5, MySql Nginx, Memcached, Zend 1

Exemplo do meu virtual host
===========================

server {
    server_name local.zend;
    
    access_log /var/log/nginx/local.zend.zend-access.log;
    
    error_log /var/log/nginx/local.zend.zend-error.log;
    root /var/www/zendframework/public;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
        index index.html index.htm index.php;
    }

    location ~ \.php$ {
        include /etc/nginx/fastcgi_params;
        fastcgi_pass  127.0.0.1:9001;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME /var/www/zendframework/public$fastcgi_script_name;
        fastcgi_param APPLICATION_ENV testing;
        fastcgi_param APPLICATION_THEME zend;
        fastcgi_read_timeout 300;
     }
}



O arquivo .sql estará dentro do diretório _offline

Criar um banco chamado *zend*
Criar um usuário chamado *zend* e atribuir permissão para o banco *zend*

O teste possui basicamente dois módulos, sendo eles /admin e /site
O email do administrador é admin@teste.com.br com senha 123456

Nesse adminstrador é algo bem simples apenas para cadasrtrar e editar, não possui exclusão. Com isso fica fácil adiconar novos conteúdos para fazer um teste.

O sistema possui Cache de Item opcional, não inclui o resto por não ter dado tempo;

